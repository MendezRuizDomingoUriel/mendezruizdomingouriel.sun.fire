<?php

$app->get('/', function() use($app) {
  $datos = array(
    'titulo' => '/',
    'recursos' => array(
      array('url' => '/api/xml', 'descripcion' => 'Censo 2009 XML'),
    )
  );
  $app->render('root.php', $datos);
});
